COMMENT BURY/PROMOTE

This simple module will allow you to do two things: bury comments which have
been voted down (using ANY ONE of the voting forms available in Drupal) and 
"promote" the comment with the highest number of votes for a node (again, using 
ANY ONE of the voting forms in Drupal).

This module doesn't do the dirty work for you: it offers you, however, simple
functions you can use in your themes to make all this happen. Here is how.

The goal is to offer both a moderation system, and a way of using Drupal
forums as a "The best community answer" site.
